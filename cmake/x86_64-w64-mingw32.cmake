set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSTEM_PROCESSOR x86_64)
set(TRIPLE "x86_64-w64-mingw32")

set(CMAKE_C_COMPILER "${TRIPLE}-gcc-posix")
set(CMAKE_CXX_COMPILER "${TRIPLE}-g++-posix")
set(CMAKE_ASM_COMPILER "${TRIPLE}-as")
set(CMAKE_AR "${TRIPLE}-ar")
set(CMAKE_NM "${TRIPLE}-nm")
set(CMAKE_STRIP "${TRIPLE}-strip")
set(CMAKE_RANLIB "${TRIPLE}-ranlib")
set(CMAKE_LINKER "${TRIPLE}-ld")
set(CMAKE_OBJCOPY "${TRIPLE}-objcopy")
set(CMAKE_READELF "${TRIPLE}-readelf")

# where is the target environment located
set(CMAKE_FIND_ROOT_PATH  /usr/${TRIPLE})

# adjust the default behavior of the FIND_XXX() commands:
# search programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# search headers and libraries in the target environment
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
